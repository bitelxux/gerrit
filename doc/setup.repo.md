# Create the git repo
```
mkdir -p /home/cnn/work/git-server/orange.git
cd /home/cnn/work/git-server/orange.git
git init --bare
```

# checkout that repo
```
cd /tmp
git clone /home/cnn/work/git-server/orange.git
```

# clone the real project from github
```
git clone https://github.com/bitelxux/orange.git orange.temp
```

# copy content to new repo and push to master
```
rm -rf orange.temp/.git
cp -r orange.temp/* orange
cd orange
git add *
git commit -m "[PR-000] First commit"
git push origin master
```

# setup two users workspaces
```
mkdir /tmp/workspace && cd /tmp/workspace
git clone /home/cnn/work/git-server/orange.git orange.user1
git clone /home/cnn/work/git-server/orange.git orange.user2

cd /tmp/workspace/orange.user1
git config user.email "user1@example.com"
git config user.name "user1"


cd /tmp/workspace/orange.user2
git config user.email "user2@example.com"
git config user.name "user2"
```
