# Download gerrit
https://gerrit-releases.storage.googleapis.com/gerrit-2.15.3.war
apt-get install openjdk-8-jre-headless git vim

# Add a public key to user admin
Has to be done through the GUI

# Move repo to gerrit
- Create a empty gerrit project
- In the local repo we want to move:
  -   git remote rm origin
  -   git remote add origin ssh://admin@gerrit:29418/orange.git
  -   git push --tags origin
  -   git push --all origin

# create user for jenkins
- In the jjenkins server, create a ssh pair for user jenkins and from the host
create the user jenkins in gerrit using the jenkins user pub key

cat resources/jenkins/ssh/id_rsa.pub ssh -p 29418 admin@gerrit gerrit create-account --group "'Non-Interactive Users'" --ssh-key - jenkins

# enable jenkins score

mkdir foo
cd foo
git init
git remote add origin ssh://admin@gerrit:29418/All-Projects
git fetch origin refs/meta/config:refs/remotes/origin/meta/config
git checkout meta/config
vim project.config

add project.config 
[label "Verified"]
       function = MaxWithBlock
       value = -1 Fails
       value =  0 No score
       value = +1 Verified

git add
git commit -m "verify"
git push origin meta/config:meta/config


Tambien hay que dar permiso en All-Projects
Reference: refs/head/*

en Label Code-Review a "Non-Interactive Users"

# Para que gerrit pueda enviar correos

en el servidor the gerrit 
etc/gerrit.config

```
[sendemail]
        from = ${user} <xxxxxx@gmail.com>
        smtpServer = smtp.gmail.com
        smtpServerPort = 587
        smtpUser = xxxxxx@gmail.com
        smtpPass = xxxxxxxxxxx
        smtpEncryption = tls
```

# Allow remote install

etc/gerrit.config:

```
[plugins]
    allowRemoteAdmin = true
```

# How to populate ldap

* Create a file like
```
dn: ou=groups,dc=example,dc=org
objectClass: organizationalUnit
objectClass: top
ou: groups

dn: ou=users,dc=example,dc=org
objectClass: organizationalUnit
objectClass: top
ou: users

dn: cn=developers,ou=groups,dc=example,dc=org
objectClass: posixGroup
objectClass: top
cn: developers
gidNumber: 1002

dn: cn=testers,ou=groups,dc=example,dc=org
objectClass: posixGroup
objectClass: top
cn: testers
gidNumber: 1002
```

* Run
```
sudo ldapadd -H ldap://ldap -c -x -D cn=admin,dc=example,dc=org -W -f ./populate.ldif
sudo ldapadd -H ldap://ldap -c -x -D cn=admin,dc=example,dc=org -w secret -f ./populate.ldif
```

# gerrit ldap conf

etc/gerrit/gerrit.config
```
[auth]
        type = ldap
[ldap]
   server = ldap://ldap
   username=cn=admin,dc=example,dc=org
   accountBase = dc=example,dc=org
   accountPattern = (&(objectClass=person)(uid=${username}))
   accountFullName = displayName
   accountEmailAddress = mail

   mandatoryGroup = ldap/developers
   groupBase = ou=groups,dc=example,dc=org

   groupPattern = (cn=${groupname})
   groupMemberPattern = (memberUid=${username})
   fetchMemberOfEagerly = true
```

etc/gerrit/secure.config

add

```
[ldap]
  password = secret
```

# Add local sshkey to user1

```
cat ~/id_rsa.pub | ssh --o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null"i resources/gerrit/id_rsa -p 29418 admin@gerrit gerrit set-account --add-ssh-key - --add-email user1@example.com user1
```

# Test commit
- from orange directory
```
watch -n 10 "echo 'test' >> README.md &&  git commit -am 'test' && git push origin HEAD:refs/for/master"
```

# jira

- en gerrit.config

[commentLink "its-jira"]
        match = ([A-Z]+-[0-9]+)
        html = <a href=\"https://jira::8080/browse/$1\">$1</a>
        association = MANDATORY
#[plugin "its-jira"]
#        enabled = true
#        association = SUGGESTED
#[plugin "its-base"]
#        association = SUGGESTED
#        enabled = true
#[its-jira]
#        url=http://jira:8080
#        username=admin
#        commentOnChangeAbandoned = true
#        commentOnChangeCreated = true
#        commentOnChangeMerged = true
#        commentOnChangeRestored = false
#        commentOnCommentAdded = true
#        commentOnFirstLinkedPatchSetCreated = true
#        commentOnPatchSetCreated = true
#        commentOnRefUpdatedGitWeb = true


--- en secure.config

[its-jira]
        password=secret

# Resolve conflict

- git review -d <changeid>
- git rebase origin/master
- git mergetool
- git add *
- git rebase --continue
- git review



