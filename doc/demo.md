user1

PR-001: Update documentation
 - creates a change branch
 - commits a change
 - cide review spots something is missing
 - commit a change
 - code review misses a typo
 - code gets merged
 - commit a fix
 - code gets merged

user2

PR-002: Add a new method
 - creates a feature branch
 - commits a change
 - review spots an error
 - rework
 - commits a change
 - review doesn't catch an error
 - code gets merged
 - master gets broken!
 - rework
 - commit and merge a change to fix it
