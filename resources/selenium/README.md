## Regular use

```docker run --name selenium -p5900:5900 -p4444:4444 --rm -d selenium```

A vnc server running on the container is reachable at localhost:5900 via any vnc client like

```xvnc4viewer localhost```

