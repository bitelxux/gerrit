#!/usr/bin/env python

import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium

PASSWORD='secret'

if __name__ == '__main__':
    manager = Selenium()


    # login
    manager.open("http://jira:8080/plugins/servlet/embedded-crowd/directories/list")
    manager.set_value('id', 'login-form-username', 'admin')
    manager.set_value('id', 'login-form-password', PASSWORD)
    manager.click('id', 'login-form-submit')
    manager.set_value('id', 'login-form-authenticatePassword', PASSWORD)
    manager.click('id', 'login-form-submit')

    # Add ldap directory
    manager.click('id', 'new-directory')
    manager.driver.find_element_by_xpath("//select[@name='newDirectoryType']/option[text()='LDAP']").click()
    manager.click('name', 'next')

    # Select type
    manager.click('id', 'configure-ldap-form-type')
    manager.driver.find_element_by_xpath("//select[@name='type']/option[text()='OpenLDAP (Read-Only Posix Schema)']").click()

    # more stuff
    manager.set_value('id', 'configure-ldap-form-hostname', 'ldap')
    manager.set_value('id', 'configure-ldap-form-ldapUserdn', 'cn=admin,dc=example,dc=org')
    manager.set_value('id', 'configure-ldap-form-ldapPassword', 'secret')
    manager.set_value('name', 'ldapBasedn', 'dc=example,dc=org')
    # Prevous set_value can be changed to 'name' too
    manager.set_value('name', 'ldapUserDn', 'ou=users')
    manager.set_value('name', 'ldapGroupDn', 'ou=groups')


    manager.click('id', 'toggle-group-schema-settings')
    manager.set_value('name', 'ldapGroupObjectclass', 'posixGroup')
    manager.set_value('name', 'ldapGroupFilter', '(objectclass=posixGroup)')
    manager.set_value('id', 'configure-ldap-form-ldapGroupDescription', 'cn')
    manager.click('id', 'configure-ldap-form-submit')


    # test
    manager.set_value('name', 'username', 'user1')

    try:
        manager.set_value('id', 'troubleshoot-directory-form-password-placeHolder', 'user1')
    except Exception:
        manager.set_value('id', 'troubleshoot-directory-form-password', 'user1')


    manager.click('name', 'test')

    # Grant access to LDAP users

    manager.open("http://jira:8080/secure/admin/ApplicationAccess.jspa")
    # mmm I need to wait a little or the widget will get only partial text
    widget = manager.set_value("id", "groupname-29-field", "developers")
    time.sleep(2)
    widget.send_keys(Keys.RETURN)
    time.sleep(2)

    # Sync LDAP
    manager.open("http://jira:8080/plugins/servlet/embedded-crowd/directories/list")
    try:
        manager.click("text", "Synchronise")
    except Exception:
        print("Synchronize button might be covered by a pop up window")

    time.sleep(5)
    manager.driver.quit()

