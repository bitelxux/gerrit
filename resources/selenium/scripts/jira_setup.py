#!/usr/bin/env python

import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium

PASSWORD='secret'

if __name__ == '__main__':
    manager = Selenium()


    # login
    manager.open("http://jira:8080")

    # Select LDAP
    manager.click('id', 'jira-setupwizard-submit')
    manager.click('id', 'generate-mac-license')
    widget = manager.set_value('id', 'username', os.getenv("ATLASIAN_USER"))
    widget.send_keys(Keys.RETURN)

    # Workaround to the script trying too soon to set password
    t = time.time()
    while time.time() - t < 180:
        try:
            widget = manager.set_value('id', 'password', os.getenv("ATLASIAN_PASSWORD"))
            widget.send_keys(Keys.RETURN)
            break
        except Exception:
            pass # not ready yet


    # select software ... to be moved to utils
    t = time.time()
    while time.time() - t < 180:
        try:
            radio = manager.driver.find_element_by_xpath("//input[@name='productLicenseType' and @id='jira-software']")
            manager.driver.execute_script("arguments[0].click();", radio)
            manager.click('id', 'generate-license')
            break
        except Exception:
            pass # not visible yet

    # Pres Yes in the dialog
    t = time.time()
    button = None
    while not button and time.time() -t < 180:
        try:
            button = manager.driver.find_element_by_xpath("//div[@class='ui-dialog-buttonset']//span[contains(.,'Yes')]")
        except Exception:
            time.sleep(1)

    if not button:
       print("Error waiting for 'Yes' button")
       sys.exit()

    button.click()

    widget = manager.driver.find_element_by_id('importLicenseForm')
    widget.submit()

    # administrator account
    manager.set_value('name', 'fullname', 'admin')
    manager.set_value('name', 'email', 'admin@example.com')
    manager.set_value('name', 'username', 'admin')
    manager.set_value('name', 'password', PASSWORD)
    manager.set_value('name', 'confirm', PASSWORD)

    button = manager.wait_for_element('id', 'jira-setupwizard-submit')
    button.click()

    # again, on setup mail notifications
    button = manager.wait_for_element('id', 'jira-setupwizard-submit')
    button.click()

    # Accept language
    button = manager.wait_for_element('id', 'next')
    button.click()

    # Skip avatar selection
    button = manager.wait_for_element('class', 'avatar-picker-done')
    button.click()

    # Create default project
    button = manager.wait_for_element('id', 'emptyProject')
    button.click()

    button = manager.wait_for_element('class', 'pt-submit-button')
    button.click()

    # "Scrum software development" explanation
    button = manager.wait_for_element('class', 'template-info-dialog-create-button')
    button.click()

    # Project name
    manager.set_value('name', 'name', 'Orange')
    button = manager.wait_for_element('class', 'add-project-dialog-create-button')
    button.click()

    # Create a test issue
    manager.click('id', 'create_link')
    manager.set_value('id', 'issuetype-field', 'Task\t')
    t = time.time()
    while time.time() -t < 30:
        try:
            manager.set_value('id', 'summary', 'Issue summary')
            break
        except Exception:
            print("Oops, not ready yet ...")
            time.sleep(1)

    manager.click('id', 'create-issue-submit')

    time.sleep(5)
    manager.driver.quit()

