#!/bin/bash

cd /tmp/scripts

echo "Setting up Jira initial configuration"
./jira_setup.py

echo "Setting up Jira LDAP integration"
./jira_ldap.py

echo "Setting up Gerrit integration"
./jira_gerrit.py
