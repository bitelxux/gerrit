#!/usr/bin/env python

import os
import sys
import time

from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium

WAIT_FOR_INITIAL_JENKINS_INSTALL_TIMEOUT = int(os.getenv("WAIT_FOR_INITIAL_JENKINS_INSTALL_TIMEOUT", 600))

if __name__ == '__main__':

    print("WAIT_FOR_INITIAL_JENKINS_INSTALL_TIMEOUT: %d" % WAIT_FOR_INITIAL_JENKINS_INSTALL_TIMEOUT)

    password = sys.argv[1]
    manager = Selenium()

    # login
    manager.open("http://jenkins:8080/login")

    # Login with initial password
    widget = manager.set_value('id', 'security-token', password)
    widget and widget.send_keys(Keys.RETURN)

    # Wait for 'Install Recommended Plugins screen'
    manager.open("http://jenkins:8080")
    widget = manager.wait_for_element("class", "install-recommended")
    widget and widget.click()

    # We have clicked "Install Recommended". Wait for it to complete
    widget = manager.wait_for_element("class", "skip-first-user|continue-with-failed-plugins", WAIT_FOR_INITIAL_JENKINS_INSTALL_TIMEOUT)
    widget and widget.click()

    widget = manager.wait_for_element("class", "skip-first-user")
    widget and widget.click()

    widget = manager.wait_for_element("class", "save-configure-instance")
    widget and widget.click()

    time.sleep(5)
    manager.driver.quit()

