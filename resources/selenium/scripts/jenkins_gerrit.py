#!/usr/bin/env python

import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium


if __name__ == '__main__':
    manager = Selenium()

    # login
    manager.open("http://jenkins:8080/pluginManager/available")
    manager.set_value('name', 'j_username', 'admin')
    widget = manager.set_value('name', 'j_password', sys.argv[1])
    widget and widget.send_keys(Keys.RETURN)

    # Install gerrit trigger plugin 
    widget = manager.set_value('id', 'filter-box', 'gerrit')
    widget and widget.send_keys(Keys.RETURN)
    manager.click('name', 'plugin.gerrit-trigger.default')
    manager.click('xpath', 'Install without restart')

    cmd = "curl -s -u admin:secret http://jenkins:8080/pluginManager/installed | grep 'Gerrit Trigger' > /dev/null" 
    t = time.time()
    res = os.system(cmd)
    while time.time() - t < 120 and res != 0:
        print("Waiting for Gerrit Trigger")
        time.sleep(5)
        res = os.system(cmd)
    if res == 0:
        print("Gerrit Trigger installed !")
    else:
        print("Gerrit Trigger NOT installed")

    # Setup gerrit
    manager.open("http://jenkins:8080/gerrit-trigger/newServer")
    manager.set_value('id', 'name', 'gerrit')
    manager.click('name', 'mode')
    manager.click('xpath', 'OK')

    manager.set_value('name', 'gerritHostName', 'gerrit')
    manager.set_value('name', 'gerritFrontEndUrl', 'http://gerrit:8080')
    manager.set_value('name', 'gerritUserName', 'jenkins')
    manager.set_value('name', 'gerritAuthKeyFile', '/var/jenkins_home/.ssh/id_rsa')
    manager.click('xpath', 'Save')

    manager.click('name', 'server')

    time.sleep(5)
    manager.driver.quit()
