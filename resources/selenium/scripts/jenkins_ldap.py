#!/usr/bin/env python

import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium


if __name__ == '__main__':
    manager = Selenium()

    # login
    manager.open("http://jenkins:8080/login")
    manager.set_value('name', 'j_username', 'admin')
    widget = manager.set_value('name', 'j_password', sys.argv[1])
    widget and widget.send_keys(Keys.RETURN)

    # Select LDAP
    manager.open("http://jenkins:8080/configureSecurity/")
    manager.click('id', 'radio-block-4')

    # Set up LDAP server
    manager.set_value('name', '_.server', 'ldap://ldap')

    # Open advanced configuration
    manager.click('id', 'yui-gen3-button')

    # Rest of LDAP conf
    manager.set_value('name', '_.rootDN', 'dc=example,dc=org')
    manager.set_value('name', '_.managerDN', 'cn=admin,dc=example,dc=org')
    manager.set_value('name', '_.managerPasswordSecret', 'secret')
    manager.set_value('name', '_.displayNameAttributeName', 'cn')
    manager.click('xpath', 'Save')

    time.sleep(5)
    manager.driver.quit()
