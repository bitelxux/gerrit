#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import requests
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

TIMEOUT = os.getenv('SELENIUM_TIMEOUT', 300)

class Selenium(object):
    def __init__(self, *args, **kw):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_argument("--no-sandbox")
        #chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(chrome_options=chrome_options, service_args=["--verbose"])
        self.driver.maximize_window()

    def open(self, url, timeout=TIMEOUT):
        t = time.time()

        req = None
        while time.time() - t < timeout:
            try:
                req = requests.get(url)
                self.driver.get(url)
                return True
            except Exception:
                time.sleep(1)

        raise ValueError('Timeout after %s seconds waiting for %s' %(timeout, url))


    def set_value(self, _type, what, value, timeout=TIMEOUT):
        widget = self.wait_for_element(_type, what, timeout)
        if widget:
            try:
                widget.clear()
            except Exception:
                pass
            widget.send_keys(value)
            return widget
        return None

    def click(self, _type, what, timeout=120):
        widget = self.wait_for_element(_type, what, timeout)
        if widget:
            widget.click()
            return widget
        return None

    def wait_for_element(self, _type, what, timeout=TIMEOUT):
        if _type == 'id':
            find = self.driver.find_element_by_id
        elif _type == 'name':
            find = self.driver.find_element_by_name
        elif _type == 'class':
            find = self.driver.find_element_by_class_name
        elif _type == 'text':
            find = self.driver.find_element_by_link_text
        elif _type == 'xpath':
            find = self.driver.find_element_by_xpath
        else:
           raise ValueError("'%s' not supported" % _type)

        t = time.time()
        widget = None
        candidates = what.split('|')
        while not widget and time.time() - t < timeout:
            for candidate in candidates:
                try:
                    print "Waiting for %s %s" % (_type, candidate)
                    if _type == 'xpath':
                        widget = self.driver.find_element_by_xpath( ".//*[contains(text(), '%s')]" % what)
                    else:
                        widget = find(candidate)
                    print "Found %s" % candidate
                except Exception:
                    time.sleep(1)

        if not widget:
            raise ValueError('Timeout after %s seconds waiting for %s' %(timeout, what))

        return widget
