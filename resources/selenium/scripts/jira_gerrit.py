#!/usr/bin/env python

import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium

PASSWORD='secret'

if __name__ == '__main__':
    manager = Selenium()

    # installation
    manager.open("http://jira:8080/plugins/servlet/upm")

    manager.set_value('id', 'login-form-username', 'admin')
    manager.set_value('id', 'login-form-password', PASSWORD)
    manager.click('id', 'login-form-submit')
    manager.set_value('id', 'login-form-authenticatePassword', PASSWORD)
    manager.click('id', 'login-form-submit')

    # Close some dialogs to make the widget visible
    try:
        manager.click('id', 'nps-acknowledgement-accept-button', timeout=5)
    except Exception:
        pass
    try:
        manager.click('class', 'icon-close', timeout=5)
    except Exception:
        pass
    try:
        manager.click('class', 'icon-close', timeout=5)
    except Exception:
        pass

    time.sleep(5)

    # Upload and install the plugin
    manager.click('id', 'upm-upload')
    manager.set_value('id', 'upm-upload-url', 'https://marketplace.atlassian.com/download/apps/1210874/version/87')
    manager.click('class', 'confirm')
    manager.click('class', 'cancel')

    # configuration

    manager.open('http://jira:8080/plugins/servlet/gerrit/admin')
    manager.set_value('id', 'sshHostname', 'gerrit')
    manager.set_value('id', 'sshUsername', 'admin')
    manager.set_value('id', 'issueSearchQuery', 'message:"[%s]"')
    manager.set_value('id', 'sshPrivateKey', '/tmp/scripts/gerrit_ssh_key')

    form = manager.wait_for_element('id', 'admin')
    if form:
        form.submit()

    time.sleep(5)
    manager.driver.quit()

