#!/usr/bin/env python

import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium


if __name__ == '__main__':
    manager = Selenium()

    password = sys.argv[1]

    # login
    manager.open("http://jenkins:8080/pluginManager/available")
    manager.set_value('name', 'j_username', 'admin')
    widget = manager.set_value('name', 'j_password', password)
    widget and widget.send_keys(Keys.RETURN)

    # Install cobertura plugin
    widget = manager.set_value('id', 'filter-box', 'theme')
    widget and widget.send_keys(Keys.RETURN)
    manager.click('name', 'plugin.simple-theme-plugin.default')
    manager.click('xpath', 'Install without restart')

    cmd = "curl -s -u admin:secret http://jenkins:8080/pluginManager/installed | grep 'Simple Theme' > /dev/null"
    t = time.time()
    res = os.system(cmd)
    while time.time() - t < 120 and res != 0:
        print("Waiting for Simple-Theme Plugin")
        time.sleep(5)
        res = os.system(cmd)
    if res == 0:
        print("Simple-Theme Plugin installed !")
    else:
        print("Simple-Theme Plugin NOT installed")

    manager.open("http://jenkins:8080/configure")

    # This is a tricky one as it's a dynamic created button.
    # Only way I have found is to:
    # - Locate the td with 'Theme elements'
    # - Get its tr parent
    # - Get buttons in tr (there's only one)


    td =  manager.driver.find_element_by_xpath("//td[text()='Theme elements']")

    tr = td.find_element_by_xpath("..")
    button = tr.find_elements_by_xpath('.//button')[0]
    button.click()

    manager.click('text', 'CSS URL')

    # Similar thing for the text box for the theme url
    t = time.time()
    while time.time() -t < 10:
        try:
            td =  manager.driver.find_element_by_xpath("//td[text()='URL of theme CSS']")
            break
        except Exception:
            time.sleep(1)

    tr = td.find_element_by_xpath("..")
    url_box = tr.find_element_by_name('_.url')

    url_box.send_keys(os.getenv("JENKINS_THEME"))

    manager.driver.find_element_by_xpath('//button[contains(text(), "Save")]').click()

    manager.driver.quit()
