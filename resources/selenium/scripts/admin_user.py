#!/usr/bin/env python

import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium

ssh_key=open(sys.argv[1]).read()

if __name__ == '__main__':

    manager = Selenium()

    # login
    manager.open("http://gerrit:8080/login")
    widget = manager.set_value('id', 'f_user', 'admin')
    widget = manager.set_value('id', 'f_pass', 'secret')
    widget and widget.send_keys(Keys.RETURN)

    # Add admin's ssh key
    manager.open("http://gerrit:8080/settings/#SSHKeys")
    widget = manager.set_value('id', 'textarea', ssh_key)
    widget and widget.send_keys(Keys.TAB + Keys.RETURN)

    time.sleep(5)
    manager.driver.quit()

