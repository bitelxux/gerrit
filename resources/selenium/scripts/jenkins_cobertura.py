#!/usr/bin/env python

import os
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium_utils import Selenium


if __name__ == '__main__':
    manager = Selenium()

    # login
    manager.open("http://jenkins:8080/pluginManager/available")
    manager.set_value('name', 'j_username', 'admin')
    widget = manager.set_value('name', 'j_password', sys.argv[1])
    widget and widget.send_keys(Keys.RETURN)

    # Install cobertura plugin
    widget = manager.set_value('id', 'filter-box', 'cobertura')
    widget and widget.send_keys(Keys.RETURN)
    manager.click('name', 'plugin.cobertura.default')
    manager.click('xpath', 'Install without restart')

    cmd = "curl -s -u admin:secret http://jenkins:8080/pluginManager/installed | grep 'Cobertura Plugin' > /dev/null" 
    t = time.time()
    res = os.system(cmd)
    while time.time() - t < 120 and res != 0:
        print("Waiting for Cobertura Plugin")
        time.sleep(5)
        res = os.system(cmd)
    if res == 0:
        print("Cobertura Plugin installed !")
    else:
        print("Cobertura Plugin NOT installed")

    manager.driver.quit()
