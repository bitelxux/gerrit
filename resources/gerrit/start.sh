#!/bin/bash

# gerrit config
git config -f /var/gerrit/etc/gerrit.config gerrit.canonicalWebUrl "${CANONICAL_WEB_URL:-http://$HOSTNAME}:8080"
git config -f /var/gerrit/etc/gerrit.config noteDb.changes.autoMigrate true
git config -f /var/gerrit/etc/gerrit.config auth.type ldap
git config -f /var/gerrit/etc/gerrit.config auth.loginText "here!"
git config -f /var/gerrit/etc/gerrit.config ldap.server ldap://ldap
git config -f /var/gerrit/etc/gerrit.config ldap.username cn=admin,dc=example,dc=org
git config -f /var/gerrit/etc/gerrit.config ldap.accountBase dc=example,dc=org
git config -f /var/gerrit/etc/gerrit.config ldap.accountPattern '(&(objectClass=person)(uid=${username}))'
git config -f /var/gerrit/etc/gerrit.config ldap.accountFullName displayName
git config -f /var/gerrit/etc/gerrit.config ldap.accountEmailAddress mail
git config -f /var/gerrit/etc/gerrit.config ldap.mandatoryGroup ldap/developers
git config -f /var/gerrit/etc/gerrit.config ldap.groupBase ou=groups,dc=example,dc=org
git config -f /var/gerrit/etc/gerrit.config ldap.groupPattern '(cn=${groupname})'
git config -f /var/gerrit/etc/gerrit.config ldap.groupMemberPattern '(memberUid=${username})'
git config -f /var/gerrit/etc/gerrit.config ldap.fetchMemberOfEagerly true

git config -f /var/gerrit/etc/secure.config ldap.password secret

# email
git config -f /var/gerrit/etc/gerrit.config sendemail.from "\${user} ${EMAIL_FROM}"
git config -f /var/gerrit/etc/gerrit.config sendemail.smtpServer ${EMAIL_SERVER}
git config -f /var/gerrit/etc/gerrit.config sendemail.smtpServerPort ${EMAIL_PORT}
git config -f /var/gerrit/etc/gerrit.config sendemail.smtpUser ${EMAIL_USER}
git config -f /var/gerrit/etc/gerrit.config sendemail.smtpPass ${EMAIL_PASSWORD}
git config -f /var/gerrit/etc/gerrit.config sendemail.smtpEncryption ${EMAIL_ENCRYPTION}

# jira
git config -f /var/gerrit/etc/gerrit.config commentLink.its-jira.match '([A-Z]+-[0-9]+)'
git config -f /var/gerrit/etc/gerrit.config commentLink.its-jira.html '<a href="http://jira:8080/browse/$1">$1</a>'
git config -f /var/gerrit/etc/gerrit.config commentLink.its-jira.association MANDATORY

git config -f /var/gerrit/etc/gerrit.config plugin.its-jira.enabled true
git config -f /var/gerrit/etc/gerrit.config plugin.its-jira.association SUGGESTED

git config -f /var/gerrit/etc/gerrit.config plugin.its-base.enabled true
git config -f /var/gerrit/etc/gerrit.config plugin.its-base.association SUGGESTED

git config -f /var/gerrit/etc/gerrit.config its-jira.url http://jira:8080
git config -f /var/gerrit/etc/gerrit.config its-jira.username admin
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnChangeAbandoned true
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnChangeCreated true
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnChangeMerged true
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnChangeRestored false
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnCommentAdded true
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnFirstLinkedPatchSetCreated true
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnPatchSetCreated true
git config -f /var/gerrit/etc/gerrit.config its-jira.commentOnRefUpdatedGitWeb true

# avatars-external ( to avoid gravatar )
git config -f /var/gerrit/etc/gerrit.config plugin.avatars-external.enabled true
git config -f /var/gerrit/etc/gerrit.config plugin.avatars-external.url 'http://webserver/avatars/${user}.png'
git config -f /var/gerrit/etc/gerrit.config plugin.avatars-external.changeUrl 'http://webserver/account.html'
git config -f /var/gerrit/etc/gerrit.config plugin.avatars-external.sizeParameter 's=${size}x${size}'

# remote admin
git config -f var/gerrit/etc/secure.config plugins.allowRemoteAdmin true

# install plugins
wget -O /var/gerrit/plugins/its-jira.jar https://gerrit-ci.gerritforge.com/view/Plugins-stable-2.16/job/plugin-its-jira-bazel-stable-2.16/lastSuccessfulBuild/artifact/bazel-bin/plugins/its-jira/its-jira.jar
wget -O /var/gerrit/plugins/its-base.jar https://gerrit-ci.gerritforge.com/view/Plugins-stable-2.16/job/plugin-its-base-bazel-stable-2.16/lastSuccessfulBuild/artifact/bazel-genfiles/plugins/its-base/its-base.jar

# Let's go !!
/var/gerrit/bin/gerrit.sh run

