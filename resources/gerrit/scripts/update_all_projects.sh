#!/bin/bash -ex

echo -e "Host gerrit\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

cd /tmp
mkdir all-projects
cd all-projects
git init
git config --global user.email "admin@example.com"
git config --global user.name "admin"

git remote add origin ssh://admin@gerrit:29418/All-Projects
git fetch origin refs/meta/config:refs/remotes/origin/meta/config
git checkout meta/config

echo '[label "Verified"]' >> project.config
echo '        function = MaxWithBlock' >> project.config
echo '        value = -1 Fails' >> project.config
echo '        value =  0 No score' >> project.config
echo '        value = +1 Verified' >> project.config
echo '        defaultValue = 0' >> project.config

sed -i "s/label-Code-Review = -1..+1 group Registered Users/label-Code-Review = -1..+1 group Registered Users\n\tlabel-Code-Review = -1..+1 group Non-Interactive Users/g" project.config

sed -i 's|\[access "refs/\*"]|\[access "refs/\*"]\n\tlabel-Verified = -1..+1 group Non-Interactive Users|g' project.config | less

git add project.config
git commit -m "Adding 'Verify' label"
git push origin meta/config:meta/config
git push

