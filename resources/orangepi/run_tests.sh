#!/bin/bash
nosetests tests --with-xunit --cover-erase --with-coverage --cover-package=orange
coverage xml --omit=tests/*
