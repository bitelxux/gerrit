#!/usr/bin/python

import mock
import os
import sys
import unittest
from mock import MagicMock
from mock import patch

current = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current + '/../orange')
sys.path.insert(0, current + './pyA20')

flask = MagicMock()
class Flask(object):
    def __init__(*args, **kw):
        pass

    @staticmethod
    def route(debug=False):
        def _route(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return _route

flask.Flask = Flask

sys.modules['flask'] = flask

import server

class TestOrange(unittest.TestCase):

    def setUp(self):
        pass

    def test_01(self):
        pass

    def test_kitchen_on(self):
        result = server.kitchen_on()
        self.assertEqual('Kitchen light is on\n', result)

    def test_kitchen_off(self):
        result = server.kitchen_off()
        self.assertEqual('Kitchen light is off\n', result)

    def test_kitchen_status(self):
        result = server.kitchen_status()
        self.assertEqual('Kitchen light is off\n', result)

    def test_living_room_on(self):
        result = server.living_room_on()
        self.assertEqual('Living room light is on\n', result)

    def test_living_room_off(self):
        result = server.living_room_off()
        self.assertEqual('Living room light is off\n', result)

    def test_living_room_status(self):
        result = server.living_room_status()
        self.assertEqual('Living room light is off\n', result)

    def test_all_on(self):
        result = server.all_on()
        self.assertEqual('All lights are on\n', result)

    def test_all_off(self):
        result = server.all_off()
        self.assertEqual('All lights are off\n', result)

    def test_all_lights_status(self):
        result = server.all_lights_status()
        self.assertEqual('Living room light is off\n'
                         'Kitchen light is off\n', result)

    """
    def test_dummy1(self):
        server.dummy1()

    def test_dummy2(self):
        server.dummy2()

    def test_dummy3(self):
        server.dummy3()

    def test_dummy4(self):
        server.dummy4()

    def test_dummy5(self):
        server.dummy5()

    def test_dummy6(self):
        server.dummy6()

    def test_dummy7(self):
        server.dummy7()

    def test_dummy8(self):
        server.dummy8()

    def test_dummy9(self):
        server.dummy9()

    def test_dummy10(self):
        server.dummy10()
    """
