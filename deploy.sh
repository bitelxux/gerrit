#!/bin/bash -e

#######################################################################################
# This section is where you might want to change some variables to fit your environment
#######################################################################################
LOCAL_USER_SSH_KEY=/home/cnn/.ssh/id_rsa.pub
TEST_PROJECT_DIR=$HOME/gerrit_playground
UPDATE_ETC_HOSTS=Yes
SELENIUM_TIMEOUT=300

CHANGE_JENKINS_THEME=Yes
JENKINS_THEME="https://cdn.rawgit.com/afonsof/\
jenkins-material-theme/gh-pages/dist/material-orange.css"

CONFIGURE_JIRA=No
ATLASIAN_USER=
ATLASIAN_PASSWORD=
#######################################################################################

# just to get a sudo window ...
sudo echo "Deploying the gerrit-jenkins-ldap-jira playground !"

BASE_DIR=$PWD

cat <<EOF >> .env
JENKINS_THEME=$JENKINS_THEME
SELENIUM_TIMEOUT=$SELENIUM_TIMEOUT
ATLASIAN_USER=$ATLASIAN_USER
ATLASIAN_PASSWORD=$ATLASIAN_PASSWORD
DISPLAY=:10
EOF

docker-compose up -d --build

selenium=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' selenium)
gerrit=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' gerrit)
jenkins=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' jenkins)
ldap_admin=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ldap-admin)
ldap=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ldap)
jira=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' jira)
webserver=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' webserver)


if [[ $UPDATE_ETC_HOSTS == 'Yes' ]]; then

  sudo sed -i '/# Gerrit playground/,/# End Gerrit playground/d' /etc/hosts
  sudo bash -c "cat <<EOT >> /etc//hosts
# Gerrit playground
$gerrit      gerrit
$jenkins     jenkins
$ldap_admin  ldap-admin
$ldap        ldap
$jira        jira
$selenium    selenium
$webserver   webserver
# End Gerrit playground
EOT
"
  selenium='selenium'
  gerrit='gerrit'
  jenkins='jenkins'
  ldap_admin='ldap-admin'
  ldap='ldap'
  jira='jira'
  webserver='webserver'
fi

[ -f ~/.ssh/config ] | touch ~/.ssh/config
grep "^Host gerrit" ~/.ssh/config || cat <<EOT >> ~/.ssh/config
Host $gerrit
     user admin
     Hostname $gerrit
     UserKnownHostsFile=/dev/null
     IdentitiesOnly=yes
     StrictHostKeyChecking=no
EOT

# Create groups in ldap

echo "Populating LDAP"

docker cp resources/ldap/populate.sh ldap:/root
docker cp resources/ldap/populate.ldif ldap:/root
docker exec ldap /root/populate.sh

echo "Doing some Selenium magic"
echo "You can have a look to what is going on with any VNC client like 'xvnc4viewer $selenium'"

echo "Waiting for selenium to set admin ssh key"

# Set gerrit admin ssh
chmod 700 resources/gerrit/ssh/id_rsa
chmod 644 resources/gerrit/ssh/id_rsa.pub
docker exec selenium env DISPLAY=:10 python /tmp/scripts/admin_user.py /tmp/scripts/id_rsa.pub &
pid=$(echo $!)

echo ""
echo "Waiting fot gerrit admin ssh key to be applied"
t=180
while ((t > 0)); do
     ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit version > /dev/null 2>&1 && break
     printf "."
     sleep 1
     ((t -= 1))
done

if [[ $t -ne 0 ]]; then
    echo "Done !"
else
    echo "Error setting gerrit admin ssh key :-/"
    exit 1
fi

# Enable 'Verified' and Non-Interactive users in All-Projects
docker exec gerrit /tmp/scripts/update_all_projects.sh

# Create non-interative jenkins user
echo ""
echo "Creating non-interactive jenkins user in gerrit"

# In case it exists from previous deployments
rm -f ./resources/jenkins/ssh/known_hosts

cat resources/jenkins/ssh/id_rsa.pub | ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit create-account --group "'Non-Interactive Users'" --ssh-key - jenkins
ssh -i resources/gerrit/ssh/id_rsa -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -p 29418 admin@$gerrit gerrit set-members -a jenkins  "'Non-Interactive Users'"

# Jenkins
echo "Setting up Jenkins. This might take a while!"
password=$(docker exec -it jenkins cat /var/jenkins_home/secrets/initialAdminPassword)
docker exec selenium env DISPLAY=:10 python /tmp/scripts/jenkins_initial_setup.py $password
if [[ $? -ne 0 ]]; then
   echo "Initial Jenkins setup Failed ! timeout?"
   exit 1
fi
pid=$(echo $!)

echo "Jenkins initial set up is done and recommended plugins should be installed"
echo "Setting up Jenkins LDAP authentification"

docker exec selenium env DISPLAY=:10 python /tmp/scripts/jenkins_ldap.py $password
pid=$(echo $!)
echo "Jenkins is now loging against LDAP. Admin user is 'admin', password 'secret'"

# Example project
echo "Setting up example project"

echo "Adding local user's public ssh key to gerrit's 'user1', 'user2','user3' and 'admin'"
echo "BASE_DIR: >>>>>>>>>>>>>>> $BASE_DIR"

cat $LOCAL_USER_SSH_KEY | ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $BASE_DIR/resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit set-account --add-ssh-key - --add-email user1@example.com user1

cat $LOCAL_USER_SSH_KEY | ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $BASE_DIR/resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit set-account --add-ssh-key - --add-email user2@example.com user2

cat $LOCAL_USER_SSH_KEY | ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $BASE_DIR/resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit set-account --add-ssh-key - --add-email user3@example.com user3

cat $LOCAL_USER_SSH_KEY | ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $BASE_DIR/resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit set-account --add-ssh-key - admin

ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i resources/gerrit/ssh/id_rsa -p 29418 admin@$gerrit gerrit create-project orange.git --description "'Example project'" --empty-commit

ssh-keyscan -p 29418 gerrit >> ~/.ssh/known_hosts

echo "Let's populate orange repo with example code ..."
mkdir -p $TEST_PROJECT_DIR
cd $TEST_PROJECT_DIR
rm -rf $TEST_PROJECT_DIR/orange
git clone ssh://admin@$gerrit:29418/orange
cd $TEST_PROJECT_DIR/orange
git config core.sshCommand "ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $BASE_DIR/resources/gerrit/ssh/id_rsa -F /dev/null"
git config user.email "admin@example.com"
git config user.name "admin"
cp -r $BASE_DIR/resources/orangepi/* .
git add .
git commit -m "First commit"
git push origin master
rm -rf $TEST_PROJECT_DIR/orange

echo "clone the example user for different users"
for user in user1 user2 user3;  do
    # Clone example project for user1
    cd $TEST_PROJECT_DIR
    mkdir -p orange && cd orange
    mkdir $user && cd $user
    git clone ssh://$user@$gerrit:29418/orange && scp -p -P 29418 $user@$gerrit:hooks/commit-msg orange/.git/hooks/
    cd orange
    git config core.sshCommand "ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $BASE_DIR/resources/gerrit/ssh/id_rsa -F /dev/null"
    git config user.email "$user@example.com"
    git config user.name "$user"
    git config gitreview.remote origin
done

echo "Continue with jenkins ... install gerrit trigger plugin"
docker exec selenium env DISPLAY=:10 python /tmp/scripts/jenkins_gerrit.py secret
if [[ $? -ne 0 ]]; then
   echo "Jenkins gerrit plugin installation failed"
   exit 1
fi

docker exec selenium env DISPLAY=:10 python /tmp/scripts/jenkins_cobertura.py secret
if [[ $? -ne 0 ]]; then
   echo "Jenkins cobertura plugin installation failed"
   exit 1
fi

docker exec selenium env DISPLAY=:10 python /tmp/scripts/jenkins_xunit.py secret
if [[ $? -ne 0 ]]; then
   echo "Jenkins xUnit plugin installation failed"
   exit 1
fi

if [[ $CHANGE_JENKINS_THEME == 'Yes' ]]; then
    docker exec selenium env DISPLAY=:10 python /tmp/scripts/jenkins_theme.py secret
    if [[ $? -ne 0 ]]; then
       echo "Jenkins Simple Theme plugin installation failed"
       exit 1
    fi
fi

echo "Copy example jobs to jenkins"
docker exec jenkins cp -r /tmp/jobs /var/jenkins_home/

# jenkins user ssh keys
# for simplicity sake, ssh keys are the same for root
# and jenkins user.
# jenkins
docker exec jenkins cp -r /tmp/ssh /var/jenkins_home/.ssh
docker exec jenkins chown -R jenkins:jenkins /var/jenkins_home/.ssh
docker exec jenkins chmod 600 /var/jenkins_home/.ssh/config
docker exec jenkins chmod 600 /var/jenkins_home/.ssh/id_rsa.pub
docker exec jenkins chmod 600 /var/jenkins_home/.ssh/id_rsa
#root
docker exec jenkins cp -r /tmp/ssh /root/.ssh
docker exec jenkins chmod 600 /root/.ssh/id_rsa.pub
docker exec jenkins chmod 600 /root/.ssh/id_rsa

docker restart jenkins

# Jira
if [[ $CONFIGURE_JIRA == 'Yes' ]]; then
  echo "Setting up Jira !!"
  docker exec selenium \
         env DISPLAY=:10 \
             ATLASIAN_USER=$ATLASIAN_USER \
             ATLASIAN_PASSWORD=$ATLASIAN_PASSWORD \
             /tmp/scripts/jira.sh
fi

docker exec selenium env DISPLAY=:10 feh -B white -x -F /tmp/done.png &
echo "All done"
echo "But before we finish ..."

echo "Some info you might need"

echo ""
echo "gerrit url: http://$gerrit:8080"
echo "  admin: secret"
echo "  user1: user1"
echo "  user2: user2"
echo "  user3: user3"
echo ""
echo "jenkins url: http://$jenkins:8080"
echo "  admin: secret"
echo "  user1: user1"
echo "  user2: user2"
echo "  user3: user3"
echo ""
echo "ldap_admin url: https://$ldap_admin"
echo "  login DN: cn=admin,dc=example,dc=org"
echo "  password: secret"
echo ""
echo "jira url: http://$jira:8080"

if [[ $CONFIGURE_JIRA == 'Yes' ]]; then
    echo "  admin: secret"
    echo "  user1: user1 (from LDAP)"
    echo "  user2: user2 (from LDAP)"
    echo "  user3: user3 (from LDAP)"
    echo ""
else
    echo "  Configuration skiped"
    echo "  See CONFIGURE_JIRA"
fi


