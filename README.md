# Gerrit PlayGround. Carlos Novo, 2019

## Motivation

Setting up a complete Gerrit playground, including Gerrit itself,
Jenkins, Jira and LDAP might not be straight forward.

I have found specially tricky the integration with LDAP.

This work is the result of my own research and troubleshootings
in order to provide a working environment where people can spend
time actually playing with Gerrit instead of dealing with the
configuration and integration.

## How to deploy the playground

The deployment is driven by de script ```deploy.sh```

Only requirements should be dockercompose.

There are a few things that need to be modified in the script
```deploy.sh``` before trying to run it:

- LOCAL_USER_SSH_KEY

It must point to the current user's public ssh key.

- TEST_PROJECT_DIR

The script will automaticaly check out the test project for
users ```user1``` and ```user2```. These are fake users, they
don't need to exist in the system. ```TEST_PROJECT_DIR``` default
is $HOME/gerrit_workspace. Feel free to change it according to your
 preferences.

- UPDATE_ETC_HOSTS

If set to ```Yes```, the script will inspect the created containers
and will update /etc/hosts accordingly.

- CHANGE_JENKINS_THEME

Not needed at all! Yet, defaulted to ```Yes``` just because it
looks better. Feel free to change it to ```No``` if you want to
safe some time or if it is causing any problems.

- CONFIGURE_JIRA

If set to ```Yes```, Jira will be configured. A default ```Orange```
project will be created and LDAP integration will be setup.

Also, Gerrit plugin will be installed and configured.

Bear in mind that you'll need an account at my.atlassian.com in order to
be able to generate and install an evaluation license.

- ATLASIAN_USER

The user at my.atlassian.com

- ATLASIAN_PASSWORD

The password at my.atlassian.com


## Setup

The ```deploy.sh``` script will deploy a dockercompose and will run
a batch of other scripts, some in the containers, some in selenium,
to set up the whole thing.

It takes on a Dell XPS 13 about 23 minutes ... including polling the
images.

The layout of the system is:

### Gerrit

The gerrit server itself.

gerrit url: http://gerrit:8080

- admin: secret
- user1: user1
- user2: user2
- user3: user3

### Jenkins

A Jenkins server to run validations.

jenkins url: http://jenkins:8080

- admin: secret
- user1: user1
- user2: user2
- user3: user3

### LDAP server

The LDAP server against authetications are done.

### LDAP Admin host

ldap_admin url: https://ldap-admin

- login DN: cn=admin,dc=example,dc=org
- password: secret

### Jira

jira url: http://jira:8080

- admin: admin (local)
- user1: user1 (from LDAP)
- user2: user2 (from LDAP)
- user3: user3 (from LDAP)

### Webserver

Just a comodity server, only providing avatars for Gerrit.

### Selenium

Selenium does a lot of the magic configuring the servers.
Once the ```selenium``` contaier has been instantiated,
you can have a look to what it is doing with any vnc client
like ```xvnc4viewer selenium```

That will help, too to troubleshoot the deployment in the case
that it fails.

```
+----------------------------------------------------------------+
|                                                                |
|                        +-----------+           DockerCompose   |
|                        |           |                           |
|                        | Webserver |                           |
|                        |           |                           |
|                        +-----+-----+                           |
|                              |                                 |
|                              |                                 |
|    +-----------+        +----+-----+        +----------+       |
|    |           |        |          |        |          |       |
|    |   Jira    +--------+  Gerrit  +--------+  Jenkins |       |
|    |           |        |          |        |          |       |
|    +-----+-----+        +----+-----+        +----+-----+       |
|          |                   |                   |             |
|          |                   |                   |             |
|          +-------------------+-------------------+             |
|                              |                                 |
|                         +----+-----+                           |
|                         |          |                           |
|                         | OpenLDAP |                           |
|                         |          |                           |
|                         +----+-----+        +----------+       |
|                              |              |          |       |
|                              |              | Selenium |       |
|                         +----+-----+        |          |       |
|                         |          |        +----------+       |
|                         | OpenLDAP |                           |
|                         |   adm    |                           |
|                         +----------+                           |
|                                                                |
|                                                                |
+----------------------------------------------------------------+
```

## Troubleshooting

I have found out that sometimes the installation might fail.

Again, the script output and a vnc client will help you to figure
out what was wrong.

Unfortunately, the deployment is not idempotent, so in the case that
you need to re-run it the better you destroy the dockercompose and
associated volumes  with

```sudo docker-compose down; sudo docker volume prune -f```

Careful with that ```docker volume prune -f``` though !! If you have
any other docker volumes that are not in use but you want to keep,
may be you want to delete the right volumes.

## Next

Once the deployment has finished, you are good to run some test.

Go to the directory where the example project was cloned;
by default, example project is cloned at:

- $HOME/gerrit_workspace/orange/user1/orange
- $HOME/gerrit_workspace/orange/user2/orange
- $HOME/gerrit_workspace/orange/user3/orange

And make some change

- Edit a file
- git add <file>
- git commit -m "[OR-1] first test"
- git push origin HEAD:refs/for/master

If everything is right, a job will be triggered in Jenkins and
it will provide ```+1``` or ```-1``` depending on whether the job succeeds or not.

Also, if ```CONFIGURE_JIRA``` was set to ```Yes```, the jira issue ```OR-1```
will be updated with the result.

## TODO

- To make the deployment more robust. It tends to fail, probably to
  timing issues with selenium. For now, and for my own use I'm happy
  enough just replaying the whole thing but that shold be fixed.

- Stick versions! The whole thing could fail if new incompatible docker
  images are pulled.

- Tidy code. It might be a little bit messy at the moment.

- Some of the selenium scripts, use selenium directly for features
  that are not supported in selenium_utils ... right thing would be
  to move them to selenium_utils

- Jenkins setup is not very efficient. First of all, it goes through the
  'Install Recommended Plugins' and it takes very long. That should
  be changed to install only the required plugins.

- And may be all of them at the same time, instead of going through the
  whole thing for each of them.

- Email is not setup. I'm thinking of adding another container with a mail
server ... some day

## Finally

This deployment has been tested on

- Linux Mint 19
- Ubuntu 16

Gerrit is a great tool. Enjoy !














